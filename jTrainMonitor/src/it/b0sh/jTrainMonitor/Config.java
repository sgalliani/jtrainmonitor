package it.b0sh.jTrainMonitor;

import it.b0sh.utils.Configurer;

import java.net.URL;

/**
 * Created by Simone on 08/08/2015.
 */
public class Config extends Configurer {

    protected static final Configurer config = new Config();

    public final static Property API_VIAGGIATRENO_BASEURL               	= config.new Property( "api.viaggiatreno.baseurl" );


    protected Config(){
    }

    public static void init( java.net.URL... urls ) throws Exception {
        config.load( urls );
    }

    public static void unload(){
        config.reset();
    }

}
